package com.peixoto.loja.cadastro;

public class Cliente {
	private String cpf;
	private String nome;
	private String endereco;
	private long telefone;
	private int tamanhoNome;

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public long getTelefone() {
		return telefone;
	}

	public void setTelefone(long telefone) {
		this.telefone = telefone;
	}

	public int getTamanhoNome() {
		return tamanhoNome;
	}

	public void setTamanhoNome(int tamanhoNome) {
		this.tamanhoNome = tamanhoNome;
	}

	public Cliente(String cpf, String nome, String endereco, long telefone) {
		this.cpf = cpf;
		this.nome = nome;
		this.endereco = endereco;
		this.telefone = telefone;
	}

	public Cliente() {
	}
}
