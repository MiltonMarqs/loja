package com.peixoto.loja.estoque;

public class Produto {

    private Integer codigo;
    private String nome;
    private String descricao;
    private String imagem;
    private Double valor;
    private Integer estoque;


	public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getImagem() {
        return imagem;
    }

    public void setImagem(String imagem) {
        this.imagem = imagem;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public Integer getEstoque() {
        return estoque;
    }

    public void setEstoque(Integer estoque) {

        this.estoque = estoque;
    }


    public Produto(Integer codigo, String nome, String descricao, Double valor, Integer estoque) {
        this.codigo = codigo;
        this.nome = nome;
        this.descricao = descricao;
        this.valor = valor;
        this.estoque = estoque;
    }

    public Produto() {
    }

    @Override
    public String toString() {
        return  " Produto " +
                " Codigo : " + codigo + "\n" +
                " Item : " + nome + "\n" +
                " Descricao : " + descricao + "\n" +
                " valor  PRODUTO: R$ " + valor + "\n" +
                " Estoque : " + estoque;

    }
}
