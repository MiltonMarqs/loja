package com.peixoto.loja.pedido;

import com.peixoto.loja.estoque.Produto;

public class ItemPedido {
    private Produto produto;
    private Integer quantidade;
    private Double preco;

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Double getPreco() {
        return preco;
    }

    public void setPreco(Double preco) {
        this.preco = preco;
    }

    public ItemPedido(Produto produto, Integer quantidade) {
        this.produto = produto;
        this.quantidade = quantidade;
    }

    public ItemPedido() {

    }

    @Override
    public String toString() {
        return "Itens do Pedido : "
                + "produto : " + produto + "\n"
                + " quantidade : " + quantidade + "\n"
                + " preco : R$ " + preco;
    }
}
