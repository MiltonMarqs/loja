package com.peixoto.loja.pedido;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.peixoto.loja.cadastro.Cliente;
import com.peixoto.loja.estoque.Produto;

public class Pedido {
	private Integer codigo;
	private List<ItemPedido> listaItemPedido;
	private LocalDate dataPedido;
	private Cliente cliente;
	private Produto valor;

	public Pedido() {
		listaItemPedido = new ArrayList<>();
	}

	public Integer getCodigo() {

		return codigo;
	}

	public void setCodigo(Integer codigo) {

		this.codigo = codigo;
	}

	public List<ItemPedido> getListaItemPedido() {

		return listaItemPedido;
	}

	public void setListaItemPedido(List<ItemPedido> listaItemPedido) {

		this.listaItemPedido = listaItemPedido;
	}

	public LocalDate getDataPedido() {

		return dataPedido;
	}

	public void setDataPedido(LocalDate dataPedido) {

		this.dataPedido = dataPedido;
	}

	public Cliente getCliente() {

		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Produto getValor() {

		return valor;
	}

	public void setValor(Produto valor) {

		this.valor = valor;
	}

	public void adicionaItemPedido(ItemPedido itemPedido) {

		listaItemPedido.add(itemPedido);
	}

	@Override
	public String toString() {
		return 	"Pedido :" + codigo + "\n" +
				 listaItemPedido + "\n" +
				" Data Pedido :" + dataPedido + "\n" +
				" cliente : " + cliente + "\n";

	}
}