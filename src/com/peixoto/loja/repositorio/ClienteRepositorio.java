package com.peixoto.loja.repositorio;

import java.util.ArrayList;
import java.util.List;

import com.peixoto.loja.cadastro.Cliente;

public class ClienteRepositorio {

	private static List<Cliente> listaClientes = new ArrayList<>();

	public void salvar(Cliente cliente) {
		listaClientes.add(cliente);
	}

	public List<Cliente> buscarTodos() {
		return listaClientes;
	}

	public Cliente buscarClientePorCpf(String cpf) {
		Cliente clienteEncontrado = null;
		for (Cliente clienteNaLista : listaClientes) {
			if (clienteNaLista.getCpf().equals(cpf)) {
				clienteEncontrado = clienteNaLista;
			}
		}
		return clienteEncontrado;
	}

	public List<Cliente> buscarClienteporNome(String nome) {
		List<Cliente> listaClientesPesquisa = new ArrayList<>();
		for (Cliente clienteNaLista : listaClientes) {
			if (clienteNaLista.getNome().trim().toLowerCase().contains(nome.trim().toLowerCase())) {
				listaClientesPesquisa.add(clienteNaLista);
			}
		}
		return listaClientesPesquisa;
	}
}
