package com.peixoto.loja.repositorio;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import com.peixoto.loja.pedido.ItemPedido;
import com.peixoto.loja.pedido.Pedido;

public class PedidoRepositorio {

    private static List<Pedido> listaPedido = new ArrayList<>();

    public void salvar(Pedido pedido) {
        listaPedido.add(pedido);
    }

    public List<Pedido> buscarTodos() {
        return listaPedido;
    }

    public Pedido buscarPedidoCodigo(Integer codigo) {
        Pedido pedidoEncontrado = null;
        for (Pedido pedidoNaLista : listaPedido) {
            if (pedidoNaLista.getCodigo().equals(codigo)) {
                pedidoEncontrado = pedidoNaLista;
            } else {
                System.out.println("Pedido nao encontrado.");
            }
        }
        return pedidoEncontrado;
    }

    public void decrementar(ItemPedido itemPedido, Pedido pedido) {
        itemPedido.getProduto().setEstoque(itemPedido.getProduto().getEstoque() - itemPedido.getQuantidade());
    }

    public Integer codigoPedido(Pedido pedido) {
        return listaPedido.size() + 1;
    }


    public Pedido buscarPedidoDataDepois(LocalDate data) {
        Pedido pedidoEncontrado = null;
        for (Pedido pedidoNaLista : listaPedido) {
            if (pedidoNaLista.getDataPedido().isAfter(data)){
                pedidoEncontrado = pedidoNaLista;

            }

        }
        return pedidoEncontrado;
    }


    public Pedido buscarPedidoDataAntes(LocalDate data) {
        Pedido pedidoEncontrado = null;
        for (Pedido pedidoNaLista : listaPedido) {
            if (pedidoNaLista.getDataPedido().isBefore(data)){
                pedidoEncontrado = pedidoNaLista;

            }

        }
        return pedidoEncontrado;
    }
}
