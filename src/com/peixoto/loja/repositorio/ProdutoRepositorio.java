package com.peixoto.loja.repositorio;

import com.peixoto.loja.estoque.Produto;

import java.util.ArrayList;
import java.util.List;

public class ProdutoRepositorio {

    private static List<Produto> listaProdutos = new ArrayList<>();
    private static List<Produto> produtosEmEstoque = new ArrayList<>();

    public void salvar(Produto produto) {
        listaProdutos.add(produto);
    }

    public List<Produto> buscarTodos() {
        return listaProdutos;
    }

    public List<Produto> buscarEstoque() {
        Produto estoque1 = null;
        for (Produto produtoEstoque : listaProdutos) {
            if (produtoEstoque.getEstoque() > 0) {
                estoque1 = produtoEstoque;
                produtosEmEstoque.add(estoque1);
            }
        }
        return produtosEmEstoque;
    }

    public Produto buscarProdutoPorCodigo(Integer codigo) {
        Produto produtoDoEncontrado = null;
        for (Produto produtoNaLista : listaProdutos) {
            if (produtoNaLista.getCodigo().equals(codigo)) {
                produtoDoEncontrado = produtoNaLista;
            }
        }
        return produtoDoEncontrado;
    }


    public Double buscarProdutoPorValor(Integer codigo) {
        Produto produtoDoEncontrado = null;
        for (Produto produtoNaLista : listaProdutos) {
            if (produtoNaLista.getCodigo().equals(codigo)) {
                produtoDoEncontrado = produtoNaLista;
            }
        }
        return produtoDoEncontrado.getValor();
    }

    public Integer buscarQuantidadeEstoque(Long codigo) {
        Produto produtoDoEncontrado = null;
        for (Produto produtoNaLista : listaProdutos) {
            if (produtoNaLista.getCodigo().equals(codigo)) {
                return produtoNaLista.getEstoque();
            }
        }
        return 0;
    }

}
