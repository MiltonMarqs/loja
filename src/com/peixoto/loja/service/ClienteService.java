package com.peixoto.loja.service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import com.peixoto.loja.cadastro.Cliente;

public class ClienteService {

	public String dataExpiracaoCliente(Cliente cliente) {

		LocalDate dataAgora = LocalDate.now();
		LocalDate localDate = LocalDate.now().minusDays(10).plusDays(cliente.getTamanhoNome());
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		String localDateFormat = localDate.format(formatter);

		if (dataAgora.isAfter(localDate)) {
			return "Cliente expirado em " + localDateFormat;
		} else {
			return "Cliente Apto";
		}

	}

	public String dataCliente(Cliente cliente) {
		LocalDate dataAtual = LocalDate.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		String localDateFormat = dataAtual.format(formatter);
		return localDateFormat;
	}
}



