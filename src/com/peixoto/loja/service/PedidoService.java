package com.peixoto.loja.service;

import com.peixoto.loja.estoque.Produto;
import com.peixoto.loja.pedido.ItemPedido;
import com.peixoto.loja.pedido.Pedido;

public class PedidoService {

    public Double valorTotalItem(ItemPedido itemPedido) {
        Double valorTotal = itemPedido.getQuantidade() * itemPedido.getPreco();
        return valorTotal;
    }


    public Double totalPedido(Pedido pedido) {
        Double soma = 0.0;
        for (ItemPedido itemPedido : pedido.getListaItemPedido()) {
            soma =+ valorTotalItem(itemPedido);
        }
        return soma;
    }
    // private static List<ItemPedido> listaItemPedido = new ArrayList<>();
    public ItemPedido buscarItemPorCodigo(Integer codigo, Pedido pedido) {
        ItemPedido itemEncontrado = null;
        // List<ItemPedido> listaItemPedido = new ArrayList<>();
        // Pedido pedido = new Pedido();

        for (ItemPedido itemNaLista : pedido.getListaItemPedido()) {
            if ( itemNaLista.getProduto().getCodigo().equals(codigo)) {
                itemEncontrado = itemNaLista;
            }
        }
        return itemEncontrado;
    }



}
