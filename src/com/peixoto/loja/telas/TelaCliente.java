package com.peixoto.loja.telas;

import java.util.List;
import java.util.Scanner;

import com.peixoto.loja.cadastro.Cliente;
import com.peixoto.loja.repositorio.ClienteRepositorio;
import com.peixoto.loja.service.ClienteService;
import com.peixoto.loja.service.ValidaCPF;

public class TelaCliente implements Itela {
	
	ClienteService clienteService = new ClienteService();
	final private Scanner entradaEstoque;
	private ClienteRepositorio clienteRepositorio;

	public TelaCliente(Scanner entradaEstoque) {
		this.entradaEstoque = entradaEstoque;
		this.clienteRepositorio = new ClienteRepositorio();
	}

	public void abrir() {

		
		
		System.out.println("Bem vindo ao sistema de Clientes");
		int opcao = 0;
		int opcao1;
		do {

			System.out.println("[0] - Sair do sistema");
			System.out.println("[1] - Cadastrar Cliente");
			System.out.println("[2] - Listar Clientes");
			System.out.println("[3] - Buscar Clientes");
			opcao = Integer.parseInt(this.entradaEstoque.nextLine());

			if (opcao == 1) {
				Cliente cliente = cadastrarCliente();
				this.clienteRepositorio.salvar(cliente);

			}
			if (opcao == 2) {

				List<Cliente> clientes = this.clienteRepositorio.buscarTodos();
				mostrarClientes(clientes);

			}

			if (opcao == 3) {

				System.out.println("Buscar por :");
				System.out.println("[1] - CPF");
				System.out.println("[2] - Nome");
				opcao1 = Integer.parseInt(this.entradaEstoque.nextLine());

				if (opcao1 == 1) {

					System.out.println("Informe o CPF do Cliente: ");
					String cpfClienteASerPesquiado = this.entradaEstoque.nextLine();
					Cliente clienteEncontradoNaBusca = this.clienteRepositorio
							.buscarClientePorCpf(cpfClienteASerPesquiado);
					System.out.println("Cliente: ");
					System.out.println(clienteEncontradoNaBusca.getNome() 
							+ "| " 
							+ clienteEncontradoNaBusca.getCpf());
				}

				if (opcao1 == 2) {
					System.out.println("Informe o nome do Cliente: ");
					String nomeClienteASerPesquisado = this.entradaEstoque.nextLine();
					List<Cliente> clienteEncontradoNaBusca = this.clienteRepositorio.buscarClienteporNome(nomeClienteASerPesquisado);
					System.out.println("Cliente: ");					
					for(Cliente clienteNaLista :  clienteEncontradoNaBusca  ) {						
						System.out.println(clienteNaLista.getNome() );
						System.out.println("");
					}
					
				}
			}

		} while (opcao != 0);
	}

	private void mostrarClientes(List<Cliente> clientes) {
		for (Cliente cliente : clientes) {
			System.out.println("Nome: " + cliente.getNome() 
										+ "| CPF : " 
										+ cliente.getCpf() 
										+ "| Endere�o : "
										+ cliente.getEndereco()
										+ "| Telefone : "
										+ cliente.getTelefone()
										+ "| Tamanho : "
										+ cliente.getTamanhoNome()
										+ "| Status : "
										+ clienteService.dataExpiracaoCliente(cliente)
										
										+ "\n");

		}
	}

	private Cliente cadastrarCliente() {
		Cliente cliente = new Cliente();

		System.out.println("Informe o Nome: ");
		cliente.setNome(this.entradaEstoque.nextLine());
		validaNome(cliente);
		
				
		System.out.println("Informe o CPF do Cliente: ");
		String CPF = this.entradaEstoque.nextLine();
		while(ValidaCPF.isCPF(CPF) == false) {
			System.out.println("CPF inv�lido! ");
            System.out.println("Informe o CPF do Cliente: ");
            CPF = entradaEstoque.nextLine();			
		}
		cliente.setCpf(CPF);
		
		System.out.println("Informe o endere�o do cliente: ");
		cliente.setEndereco(this.entradaEstoque.nextLine());
		System.out.println("Informe o telefone do cliente: ");
		cliente.setTelefone(Long.parseLong(this.entradaEstoque.nextLine()));
			
		return cliente;
		
	}
	
	
	public void validaNome(Cliente cliente) {
		
		if(cliente.getNome().equals(" ") || cliente.getNome().isEmpty()) {
			System.out.println("Nome Invalido");
			cadastrarCliente();
		}else {
		}
	} 
	
	
	
	
}
