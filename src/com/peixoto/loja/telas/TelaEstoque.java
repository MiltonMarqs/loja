package com.peixoto.loja.telas;

import com.peixoto.loja.estoque.Produto;
import com.peixoto.loja.repositorio.ProdutoRepositorio;

import java.util.List;
import java.util.Scanner;

public class TelaEstoque implements Itela{

    final private Scanner entradaEstoque;
    private ProdutoRepositorio produtoRepositorio;
    public TelaEstoque(Scanner entradaEstoque) {
        this.entradaEstoque = entradaEstoque;
        this.produtoRepositorio = new ProdutoRepositorio();
    }

    public void abrir() {

        System.out.println("Bem vindo ao sistema de Estoque amigo Adm");
        int opcao = 0;
        do {
            System.out.println("0 - Sair do sistema de estoque");
            System.out.println("1 - Cadastrar Produto");
            System.out.println("2 - Listar Produtos");
            System.out.println("3 - Estoque disponivel do Produto");
            
            opcao = Integer.parseInt(this.entradaEstoque.nextLine());
            if(opcao == 1) {
                Produto produto = entrarProdutoNaTela();
                this.produtoRepositorio.salvar(produto);
            }
            if(opcao == 2) {
                List<Produto> produtos = this.produtoRepositorio.buscarTodos();
                mostrarProdutos(produtos);
            }
            if (opcao == 3) {
                System.out.println("Informe o codigo do produto: ");
                Integer codigoProdutoASerPesquisado = Integer.parseInt(this.entradaEstoque.nextLine());
                Produto produtoDoEncontradoNaBusca = this.produtoRepositorio.buscarProdutoPorCodigo(codigoProdutoASerPesquisado);
                System.out.println("O Estoque : ");
                System.out.println(produtoDoEncontradoNaBusca.getEstoque());
            }

        } while (opcao != 0);

    }

    private void mostrarProdutos(List<Produto> produtos) {
        for(Produto produto : produtos) {
            System.out.println("Nome: " + produto.getNome() + " | Valor: " + produto.getValor() + " | Quantidade estoque: " + produto.getEstoque() + "\n" );
        }
    }

   private Produto entrarProdutoNaTela() {
        Produto produto = new Produto();
        System.out.println("Informe o Codigo: ");
        produto.setCodigo(Integer.parseInt(this.entradaEstoque.nextLine()));
        System.out.println("Informe o Nome: ");
        produto.setNome(this.entradaEstoque.nextLine());
        System.out.println("Informe a Descricao: ");
        produto.setDescricao(this.entradaEstoque.nextLine());
        System.out.println("Informe o Valor: ");
        produto.setValor(Double.parseDouble(this.entradaEstoque.nextLine()));
        System.out.println("Informe o Estoque: ");
        produto.setEstoque(Integer.parseInt(this.entradaEstoque.nextLine()));
        return produto;
    }


}
