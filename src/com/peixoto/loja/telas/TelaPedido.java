package com.peixoto.loja.telas;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Scanner;

import com.peixoto.loja.cadastro.Cliente;
import com.peixoto.loja.estoque.Produto;
import com.peixoto.loja.pedido.ItemPedido;
import com.peixoto.loja.pedido.Pedido;
import com.peixoto.loja.repositorio.ClienteRepositorio;
import com.peixoto.loja.repositorio.PedidoRepositorio;
import com.peixoto.loja.repositorio.ProdutoRepositorio;
import com.peixoto.loja.service.ClienteService;
import com.peixoto.loja.service.PedidoService;

public class TelaPedido implements Itela {

	final private Scanner entradaEstoque;
	private ProdutoRepositorio produtoRepositorio;
	private PedidoRepositorio pedidoRepositorio;
	private ClienteRepositorio clienteRepositorio;
	private ClienteService clienteService;
	private Pedido pedido;
	private Produto produto;
	private ItemPedido itemPedido;
	private PedidoService pedidoService;

	public TelaPedido(Scanner entradaEstoque) {
		this.entradaEstoque = entradaEstoque;
		this.produtoRepositorio = new ProdutoRepositorio();
		this.pedidoRepositorio = new PedidoRepositorio();
		this.clienteRepositorio = new ClienteRepositorio();
		this.clienteService = new ClienteService();
		this.pedido = new Pedido();
		this.produto = new Produto();
		this.itemPedido = new ItemPedido();
		this.pedidoService = new PedidoService();

	}

	public void abrir() {
		LocalDate localDate = LocalDate.now();
		Produto produto1 = new Produto(1, "hav", "havaianas", 50.0, 600);
		this.produtoRepositorio.salvar(produto1);
		Produto produto2 = new Produto(2, "tv", "tv", 2000.0, 50);
		this.produtoRepositorio.salvar(produto2);

		Cliente cliente1 = new Cliente("08587174681", "Milton", "alameda", 349874654);
		this.clienteRepositorio.salvar(cliente1);

		System.out.println("Bem vindo ao sistema de Estoque amigo Adm");
		int opcao = 0;
		int opcao1 = 0;

		do {
			System.out.println("[0] - Sair do sistema de Pedidos");
			System.out.println("[1] - Fazer um pedido");
			System.out.println("[2] - Listar produtos");
			System.out.println("[3] - Buscar Pedidos");
			Scanner sc = new Scanner(System.in);
			opcao = Integer.parseInt(this.entradaEstoque.nextLine());

			if (opcao == 1) {

				System.out.println("Informe seu CPF : ");
				String cpfClienteASerPesquiado = this.entradaEstoque.nextLine();
				ClienteRepositorio clienteRepositorio = new ClienteRepositorio();
				Cliente clienteEncontradoNaBusca = clienteRepositorio.buscarClientePorCpf(cpfClienteASerPesquiado);
				System.out.println("Bem vindo,  " + clienteEncontradoNaBusca.getNome());
				System.out.println("[1] - Fazer um Pedido");
				opcao1 = Integer.parseInt(this.entradaEstoque.nextLine());

				if (opcao1 == 1) {
					Pedido pedido1 = new Pedido();

					List<Produto> produto = this.produtoRepositorio.buscarEstoque();
					mostrarProdutos(produto);

					int x = 1;

					do {

						ItemPedido itemPedido = new ItemPedido();
						System.out.println("Informe o codigo do item: ");
						Integer codigoProduto = Integer.parseInt(entradaEstoque.nextLine());

						Produto produtoPedido = produtoRepositorio.buscarProdutoPorCodigo(codigoProduto);
						itemPedido.setPreco(produtoRepositorio.buscarProdutoPorValor(codigoProduto));

						System.out.print("Informe a quantidade do produto : ");
						itemPedido.setQuantidade(Integer.parseInt(this.entradaEstoque.nextLine()));

						itemPedido.setProduto(produtoPedido);

						pedido1.adicionaItemPedido(itemPedido);

						pedidoRepositorio.decrementar(itemPedido, pedido1);

						//adicionando os parametros ao pedido
						pedido1.setCodigo(this.pedidoRepositorio.codigoPedido(pedido1));
						pedido1.setCliente(this.clienteRepositorio.buscarClientePorCpf(cpfClienteASerPesquiado));
						pedido1.setDataPedido(localDate);

						pedidoService.totalPedido(pedido1);

						System.out.print("Deseja adicioncar mais itens ao carrinho? 1 para Sim // 2 para alterar o item: ");
						x = sc.nextInt();

						if(x == 2){

							System.out.println("Digite o item que deseja alterar: ");
							Integer codigoProdutoAlterado = Integer.parseInt(entradaEstoque.nextLine());
							List<ItemPedido> listaItemPedido = pedido1.getListaItemPedido();
							for (ItemPedido  itemPedido1 :  listaItemPedido){
								listaItemPedido.remove(this.pedidoService.buscarItemPorCodigo(codigoProdutoAlterado, pedido1));
								itemPedido.getProduto().setEstoque(itemPedido.getProduto().getEstoque());
							}

						}

					} while (x == 1);

					this.pedidoRepositorio.salvar(pedido1);

					System.out.println("");
					System.out.println("Seu pedido foi finalizado.");
					System.out.println("Codigo do pedido : " + pedido1.getCodigo()
										+ "| Cliente: " + pedido1.getCliente().getNome()
										+ "| Total : R$ " + pedidoService.totalPedido(pedido1));
				}
			}

			if (opcao == 2) {

				produtoRepositorio.buscarTodos();
				List<Produto> produto = this.produtoRepositorio.buscarTodos();
				mostrarProdutos(produto);
			}

			// Pesquisar pedido por Codigo
			if (opcao == 3) {

				System.out.println("[1] - Para pesquisar por codigo");
				System.out.println("[2] - Para pesquisar por data Apos");
				System.out.println("[3] - Para pesquisar por data Antes");
				opcao1 = sc.nextInt();

				if (opcao1 == 1) {
					System.out.println("Digite o codigo do pedido: ");
					Integer x = sc.nextInt();
					Pedido pedido = this.pedidoRepositorio.buscarPedidoCodigo(x);
					System.out.println(pedido);
				}

				// Pesquisar pedido apos a data
				if (opcao1 == 2) {
					System.out.println("Informe a data do pedido: ");
					String dataPesquisa = this.entradaEstoque.nextLine();
					DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
					LocalDate dataConsulta = LocalDate.parse(dataPesquisa, formatter);
					Pedido pedido = pedidoRepositorio.buscarPedidoDataDepois(dataConsulta);
					System.out.println(pedido);
				}

				// Pesquisar pedido antes da data
				if (opcao1 == 3) {
					System.out.println("Informe a data do pedido: ");
					String dataPesquisa = this.entradaEstoque.nextLine();
					DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
					LocalDate dataConsulta = LocalDate.parse(dataPesquisa, formatter);
					Pedido pedido = pedidoRepositorio.buscarPedidoDataAntes(dataConsulta);
					System.out.println(pedido);
				}
			}
		}
			while (opcao != 0) ;
		}

		private void mostrarProdutos (List < Produto > produtoList) {
			for (Produto produto : produtoList) {
				System.out.println("Codigo " + "[" + produto.getCodigo() + "]"
						+ " | Nome: " + produto.getNome()
						+ " | Valor: " + produto.getValor()
						+ " | Quantidade estoque: " + produto.getEstoque() + "\n");
			}
		}

//	public void mostrarPedido(List<Pedido> PedidoList) {
//		for (Pedido pedido : PedidoList) {
//			System.out.println("Codigo "	+ "[" + pedido.getCodigo()
//											+ "]" + pedido.getCliente() + pedido.getDataPedido()
//											+ " | Valor: " + pedido.getValor()
//											+ "\n");
//		}
//	}

	}

