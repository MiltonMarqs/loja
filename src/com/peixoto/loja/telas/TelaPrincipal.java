package com.peixoto.loja.telas;

import java.util.Scanner;

public class TelaPrincipal implements Itela {


    public void abrir() {
        int opcao = 0;
        Scanner entrada = new Scanner(System.in);
        System.out.println("***************Bem Vindo Amigo Administrador da Loja****************");
        do {
            System.out.println("[0] - Fechar Sistema");
            System.out.println("[1] - Abrir Tela do Estoque");
            System.out.println("[2] - Abrir Tela de Clientes");
            System.out.println("[3] - Abrir Tela de Pedidos");
                       
            opcao = Integer.parseInt(entrada.nextLine());
            if(opcao == 1) {
                TelaEstoque telaEstoque = new TelaEstoque(entrada);
                telaEstoque.abrir();
            }
            
            if(opcao == 2) {
            	TelaCliente telaCliente = new TelaCliente(entrada);
            	telaCliente.abrir();
            }
            
            if(opcao == 3) {
            	TelaPedido telaPedido = new TelaPedido(entrada);
            	telaPedido.abrir();
            }
            
        }while (opcao != 0);
        System.out.println("Fechou a Tela");
    }

}
